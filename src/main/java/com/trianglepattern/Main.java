package com.trianglepattern;

public class Main {
    public static void main(String[] args) {
        int rows = 5;
        for(int i = rows; i > 0; i--){
            for(int k = rows; k > i; k--){
                System.out.print(" ");
            }
            for(int j = i;j>0;j--){
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}


